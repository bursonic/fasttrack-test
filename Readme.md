### How to run

Just checkout, then bring docker up from project root.

`docker-compose -p ft-test up`

Application will become available on [local port 8081](http://localhost:8081)

Backend service will be running on port 3000. 

### What's inside

#### Backend

* Go 1.10.3
* Martini as HTTP framework

#### Frontend

* Vue 2
* Vuex
* Typescript
* Webpack (boilerplate config, just few customizations)
* Vue Bootstrap

#### Ops

* Docker compose
* Debian-based images
