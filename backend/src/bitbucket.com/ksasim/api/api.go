package main

import (
	"bitbucket.com/ksasim/api/controllers"
	"bitbucket.com/ksasim/api/models"
	"github.com/go-martini/martini"
	"github.com/martini-contrib/binding"
	"github.com/martini-contrib/cors"
	"github.com/martini-contrib/render"
)

func main() {

	m := martini.Classic()

	quizesCollection := models.NewQuizCollection()
	qc := controllers.NewQuizController(quizesCollection)
	ac := controllers.NewAssessmentController(quizesCollection)

	m.Use(cors.Allow(&cors.Options{
		AllowOrigins: []string{"*"},
	}))
	m.Use(render.Renderer())

	m.Get("/quizes", qc.GetQuizes)

	m.Post("/assessments", binding.Bind(models.SelectedAssessment{}), ac.Start)
	m.Get("/assessments/:id", ac.Get)
	m.Post("/assessments/:id/answers", binding.Bind([]models.AssessmentAnswer{}), ac.AnswerBatch)
	m.Post("/assessments/:aid/answers/:qid", binding.Bind(models.AssessmentAnswer{}), ac.AnswerOne)
	m.Get("/assessments/:id/result", ac.Result)

	m.Run()
}
