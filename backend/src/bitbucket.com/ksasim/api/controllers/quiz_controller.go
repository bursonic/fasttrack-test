package controllers

import (
	"bitbucket.com/ksasim/api/models"
	"github.com/martini-contrib/render"
)

type (
	QuizController struct {
		quizCollection *models.QuizCollection
	}
)

func NewQuizController(collection *models.QuizCollection) *QuizController {
	return &QuizController{collection}
}

func (qc *QuizController) GetQuizes(r render.Render) {
	r.JSON(200, qc.quizCollection.Publish())
}
