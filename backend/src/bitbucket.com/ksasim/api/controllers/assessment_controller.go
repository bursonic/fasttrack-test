package controllers

import (
	"bitbucket.com/ksasim/api/models"
	"github.com/go-martini/martini"
	"github.com/martini-contrib/render"
	"strconv"
)

type AssessmentController struct {
	runner *models.AssessmentRunner
}

func NewAssessmentController(collection *models.QuizCollection) *AssessmentController {
	return &AssessmentController{models.NewAssessmentRunner(collection)}
}

func (ac *AssessmentController) Start(selected models.SelectedAssessment, r render.Render) {
	r.JSON(201, ac.runner.Start(selected.QuizId))
}

func (ac *AssessmentController) Get(params martini.Params, r render.Render) {
	id, err := strconv.Atoi(params["id"])

	if err != nil {
		panic(err)
	}

	r.JSON(200, ac.runner.GetAssessment(id))
}

func (ac *AssessmentController) AnswerOne(params martini.Params, answer models.AssessmentAnswer, r render.Render) {
	aid, err := strconv.Atoi(params["aid"])

	if err != nil {
		panic(err)
	}

	ac.runner.Answer(aid, answer)

	r.JSON(201, 1)
}

func (ac *AssessmentController) AnswerBatch(params martini.Params, answers []models.AssessmentAnswer, r render.Render) {
	id, err := strconv.Atoi(params["id"])

	if err != nil {
		panic(err)
	}

	for _, answer := range answers {
		ac.runner.Answer(id, answer)
	}

	r.JSON(201, len(answers))
}

func (ac *AssessmentController) Result(params martini.Params, r render.Render) {
	id, err := strconv.Atoi(params["id"])

	if err != nil {
		panic(err)
	}

	r.JSON(200, ac.runner.Check(id))
}
