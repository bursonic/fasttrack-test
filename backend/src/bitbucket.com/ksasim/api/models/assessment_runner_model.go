package models

import (
	"math"
)

type AssessmentRunner struct {
	quizCollection *QuizCollection
	assessments    []Assessment
}

type AssessmentResult struct {
	CorrectCount   int
	BetterThanPcnt int
}

type SelectedAssessment struct {
	QuizId int
}

func NewAssessmentRunner(collection *QuizCollection) *AssessmentRunner {
	return &AssessmentRunner{collection, []Assessment{}}
}

func (runner *AssessmentRunner) Start(quizId int) Assessment {
	quiz, err := runner.quizCollection.GetById(quizId)

	if err != nil {
		panic(err)
	}

	assessment := Assessment{}

	assessment.Id = len(runner.assessments) + 1
	assessment.Title = quiz.Title
	assessment.QuizId = quiz.Id
	assessment.Result = 0

	for _, question := range quiz.Questions {
		assessmentQuestion := AssessmentQuestion{
			question.Id,
			question.Content,
			question.Answers,
			0,
		}

		assessment.Answers = append(assessment.Answers, assessmentQuestion)
	}

	runner.assessments = append(runner.assessments, assessment)

	return assessment
}

func (runner *AssessmentRunner) Answer(assessmentId int, answer AssessmentAnswer) {
	runner.assessments[assessmentId-1].Answer(answer)
}

func (runner *AssessmentRunner) Check(assessmentId int) AssessmentResult {
	quiz, err := runner.quizCollection.GetById(runner.assessments[assessmentId-1].QuizId)

	if err != nil {
		panic(err)
	}

	runner.assessments[assessmentId-1].Check(quiz)

	result := AssessmentResult{}
	result.CorrectCount = runner.assessments[assessmentId-1].Result
	result.BetterThanPcnt = runner.GetBetterPcnt(assessmentId, result.CorrectCount)
	return result
}

func (runner *AssessmentRunner) GetAssessment(assessmentId int) Assessment {
	return runner.assessments[assessmentId-1]
}

func (runner *AssessmentRunner) GetBetterPcnt(assessmentId int, correctCount int) int {
	if len(runner.assessments) <= 1 {
		return -1
	}

	betterThanCount := 0

	for _, assessment := range runner.assessments {
		if assessment.Id == assessmentId {
			continue
		}

		if assessment.Result < correctCount {
			betterThanCount++
		}
	}

	return int(math.Round(100 * float64(betterThanCount) / float64(len(runner.assessments)-1)))
}
