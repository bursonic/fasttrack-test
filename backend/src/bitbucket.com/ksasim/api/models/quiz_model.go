package models

import "errors"

type QuizAnswer struct {
	Id      int
	Content string
}

type QuizQuestion struct {
	Id              int
	Content         string
	Answers         []QuizAnswer
	CorrectAnswerId int
}

type Quiz struct {
	Id        int
	Title     string
	Questions []QuizQuestion
}

type PublishedQuiz struct {
	Id             int
	Title          string
	QuestionsCount int
}

func (quiz *Quiz) Publish() PublishedQuiz {
	return PublishedQuiz{quiz.Id, quiz.Title, len(quiz.Questions)}
}

type QuizCollection struct {
	items []Quiz
}

func NewQuizCollection() *QuizCollection {
	collection := &QuizCollection{}
	collection.Init()

	return collection
}

func (qc *QuizCollection) Init() {

	qc.items = []Quiz{
		{
			Id:    1,
			Title: "World capitals",
			Questions: []QuizQuestion{
				{
					Id:      1,
					Content: "Pick capital of Malta",
					Answers: []QuizAnswer{
						{1, "Sliema"},
						{2, "Valetta"},
						{3, "Birkirkara"},
						{4, "Mosta"},
					},
					CorrectAnswerId: 2,
				},
				{
					Id:      2,
					Content: "Pick capital of Italy",
					Answers: []QuizAnswer{
						{5, "Milan"},
						{6, "Palermo"},
						{7, "Rome"},
						{8, "Venice"},
					},
					CorrectAnswerId: 7,
				},
				{
					Id:      3,
					Content: "Pick capital of France",
					Answers: []QuizAnswer{
						{9, "Paris"},
						{10, "Lion"},
						{11, "Nantes"},
						{12, "Cannes"},
					},
					CorrectAnswerId: 9,
				},
			},
		},
	}
}

func (qc *QuizCollection) GetById(id int) (Quiz, error) {
	var emptyQuiz Quiz

	for _, quiz := range qc.items {
		if quiz.Id == id {
			return quiz, nil
		}
	}

	return emptyQuiz, errors.New("Quiz not found")
}

func (qc *QuizCollection) Publish() []PublishedQuiz {
	result := []PublishedQuiz{}

	for _, quiz := range qc.items {
		result = append(result, quiz.Publish())
	}

	return result
}
