package models

type AssessmentQuestion struct {
	QuestionId  int
	Content     string
	QuizAnswers []QuizAnswer
	AnswerId    int
}

type Assessment struct {
	Id      int
	QuizId  int
	Title   string
	Answers []AssessmentQuestion
	Result  int
}

type AssessmentAnswer struct {
	QuestionId int
	AnswerId   int
}

func (assessment *Assessment) Answer(answer AssessmentAnswer) {
	for index, assessmentAnswer := range assessment.Answers {
		if answer.QuestionId == assessmentAnswer.QuestionId {
			assessment.Answers[index].AnswerId = answer.AnswerId
			break
		}
	}
}

func (assessment *Assessment) Check(quiz Quiz) int {
	assessment.Result = 0

	for _, assessmentAnswer := range assessment.Answers {
		for _, quizQuestion := range quiz.Questions {
			if assessmentAnswer.QuestionId == quizQuestion.Id {
				if assessmentAnswer.AnswerId == quizQuestion.CorrectAnswerId {
					assessment.Result++
				}
			}
		}
	}

	return assessment.Result
}
