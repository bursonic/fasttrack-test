import Vue from 'vue'
import VueRouter, { Location, Route, RouteConfig } from 'vue-router'

const homeComponent = () => import('./components/home').then(({ HomeComponent }) => HomeComponent)
const assessmentComponent = () => import('./components/assessment').then(({ AssessmentComponent }) => AssessmentComponent)
const assessmentResultComponent = () => import('./components/assessment-result').then(({ AssessmentResultComponent }) => AssessmentResultComponent)

Vue.use(VueRouter)

export const createRoutes: () => RouteConfig[] = () => [
  {
    path: '/',
    component: homeComponent
  },
  {
    path: '/assessment/:id',
    component: assessmentComponent
  },
  {
    path: '/assessment/:id/results',
    component: assessmentResultComponent
  }
]

export const createRouter = () => new VueRouter({ mode: 'history', routes: createRoutes() })
