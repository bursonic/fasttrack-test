import Vue from 'vue'
import { createRouter } from './router'
import { default as makeStore } from './store'

const navbarComponent = () => import('./components/navbar').then(({ NavbarComponent }) => NavbarComponent)

import './sass/main.scss'

// tslint:disable-next-line:no-unused-expression
new Vue({
  el: '#app-main',
  router: createRouter(),
  store: makeStore(),
  components: {
    'navbar': navbarComponent
  }
})
