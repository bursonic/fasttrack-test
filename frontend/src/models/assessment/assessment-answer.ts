import { QuizAnswer } from './quiz-answer'

export class AssessmentAnswer {
  AnswerId: number
  Content: string
  QuestionId: number
  QuizAnswers: Array<QuizAnswer>
}
