import { AssessmentAnswer } from './assessment-answer'

export class Assessment {
  Id: number
  QuizId: number
  Title: string
  Result: number
  Answers: Array<AssessmentAnswer>
}
