import Vue from 'vue'
import Vuex from 'vuex'
import { makeQuizesModule } from './modules/quizes'
import { makeAssessmentModule } from './modules/assesment'

const debug = process.env.NODE_ENV !== 'production'

Vue.use(Vuex)

export default function makeStore () {
  return new Vuex.Store({
    modules: {
      quizes: makeQuizesModule(),
      assessments: makeAssessmentModule()
    },
    strict: debug
  })
}
