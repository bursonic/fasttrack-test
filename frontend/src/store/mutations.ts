export const QuizListMutations = {
  ADD: 'quizListAdd',
  CLEAR: 'quizListClear'
}

export const AssessmentMutations = {
  LOAD: 'assessmentLoad',
  ANSWER: 'assessmentAnswer',
  RESULT: 'assessmentResult'
}
