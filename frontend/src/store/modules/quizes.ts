import { QuizItem } from '../../models/quiz/quiz-item'
import { ActionContext, ActionTree, GetterTree } from 'vuex'
import { QuizListMutations } from '../mutations'
import axios from 'axios'

export function makeQuizesModule () {
  return {
    state: new State(),
    getters: getters,
    mutations: mutations,
    actions: actions
  }
}

class State {
  list: Array<QuizItem> = []
}

const getters = {
  quizListItems (state: State): Array<QuizItem> {
    return state.list
  }
} as GetterTree<State, any>

const mutations = {
  [QuizListMutations.ADD] (state: State, item: QuizItem) {
    state.list.push(item)
  },
  [QuizListMutations.CLEAR] (state: State) {
    state.list = []
  }
}

const actions = {
  loadQuizList (store: ActionContext<State, any>) {
    store.commit(QuizListMutations.CLEAR)

    axios.get('http://localhost:3000/quizes')
      .then(
        (response) => {
          response.data.forEach((quiz) => {
            store.commit(QuizListMutations.ADD, quiz)
          })
        },
        (error) => { console.log(error) }
      )
  }
} as ActionTree<State, any>
