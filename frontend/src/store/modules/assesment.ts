import { ActionContext, ActionTree, GetterTree } from 'vuex'
import axios from 'axios'
import { AssessmentMutations } from '../mutations'
import { Assessment } from '../../models/assessment/assessment'
import { AssessmentAnswer } from '../../models/assessment/assessment-answer'
import { AssessmentResult } from '../../models/assessment/assessment-result'

export function makeAssessmentModule () {
  return {
    state: new State(),
    actions: actions,
    mutations: mutations,
    getters: getters
  }
}

class State {
  started: boolean = false
  result: AssessmentResult = null
  assessment: Assessment = null
}

const getters = {
  assessmentStarted (state: State): boolean {
    return state.started
  },
  assessmentTitle (state: State): string {
    return state.assessment.Title
  },
  assessmentQuestions (state: State): Array<AssessmentAnswer> {
    return state.assessment.Answers
  },
  assessmentResult (state: State): AssessmentResult {
    return state.result
  }
} as GetterTree<State, any>

const mutations = {
  [AssessmentMutations.LOAD] (state: State, assessment: Assessment) {
    state.assessment = assessment
    state.started = true
  },

  [AssessmentMutations.ANSWER] (state: State, payload) {
    state.assessment.Answers.some((answer: AssessmentAnswer) => {
      if (answer.QuestionId === payload.QuestionId) {
        answer.AnswerId = payload.AnswerId
        return true
      }
      return false
    })
  },

  [AssessmentMutations.RESULT] (state: State, result: AssessmentResult) {
    state.result = result
  }
}

const actions = {
  startAssessment (store: ActionContext<State, any>, payload) {
    let data = { QuizId: payload.item.Id }

    axios.post('http://localhost:3000/assessments', data)
      .then(
        (response) => {
          store.commit(AssessmentMutations.LOAD, response.data)
          const assessmentId = response.data.Id
          payload.router.push({ path: `/assessment/${assessmentId}` })
        },
        (error) => { console.log(error) }
      )
  },

  loadAssessment (store: ActionContext<State, any>, payload) {
    axios.get('http://localhost:3000/assessments/' + payload.id)
      .then(
        (response) => {
          store.commit(AssessmentMutations.LOAD, response.data)
          if (response.data.Result !== 0) {
            payload.router.push({ path: `/assessment/${payload.id}/results` })
          }
        },
        (error) => { console.log(error) }
      )
  },

  postAssessment (store: ActionContext<State, any>, router) {
    let checkBatch = []

    store.state.assessment.Answers.forEach((answer: AssessmentAnswer) => {
      checkBatch.push({ QuestionId: answer.QuestionId, AnswerId: answer.AnswerId })
    })

    axios.post('http://localhost:3000/assessments/' + store.state.assessment.Id + '/answers', checkBatch)
      .then(
        (response) => {
          router.push({ path: `/assessment/${store.state.assessment.Id}/results` })
        },
        (error) => { console.log(error) }
      )
  },

  checkAssessment (store: ActionContext<State, any>) {
    axios.get('http://localhost:3000/assessments/' + store.state.assessment.Id + '/result')
      .then(
        (response) => {
          store.commit(AssessmentMutations.RESULT, response.data)
        },
        (error) => { console.log(error) }
      )
  }

} as ActionTree<State, any>
