import Component from 'vue-class-component'
import bContainer from 'bootstrap-vue/es/components/layout/container'
import bCol from 'bootstrap-vue/es/components/layout/col'
import bRow from 'bootstrap-vue/es/components/layout/row'
import bButton from 'bootstrap-vue/es/components/button/button'
import Vue from 'vue'
import { Getter } from 'vuex-class'
import { AssessmentResult } from '../../models/assessment/assessment-result'

@Component({
  template: require('./assessment-result.html'),
  components: {
    'b-container': bContainer,
    'b-col': bCol,
    'b-row': bRow,
    'b-button': bButton
  }
})

export class AssessmentResultComponent extends Vue {
  @Getter assessmentStarted: boolean
  @Getter assessmentTitle: string
  @Getter assessmentResult: AssessmentResult

  mounted () {
    if (!this.assessmentStarted) {
      this.$router.push({ path: `/assessment/${this.$route.params.id }` })
    } else {
      this.$store.dispatch('checkAssessment')
    }
  }
}
