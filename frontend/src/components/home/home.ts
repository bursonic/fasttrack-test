import { Component, Vue } from 'vue-property-decorator'
import bContainer from 'bootstrap-vue/es/components/layout/container'
import bCol from 'bootstrap-vue/es/components/layout/col'
import bRow from 'bootstrap-vue/es/components/layout/row'
import bCard from 'bootstrap-vue/es/components/card/card'
import bButton from 'bootstrap-vue/es/components/button/button'

import { Getter } from 'vuex-class'
import { QuizItem } from '../../models/quiz/quiz-item'

@Component({
  template: require('./home.html'),
  components: {
    'b-container': bContainer,
    'b-col': bCol,
    'b-row': bRow,
    'b-card': bCard,
    'b-button': bButton
  }
})
export class HomeComponent extends Vue {
  @Getter quizListItems: Array<QuizItem>

  mounted () {
    this.$store.dispatch('loadQuizList')
  }

  start (quiz) {
    this.$store.dispatch('startAssessment', { item: quiz, router: this.$router })
  }
}
