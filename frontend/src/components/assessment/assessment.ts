import Component from 'vue-class-component'
import bContainer from 'bootstrap-vue/es/components/layout/container'
import bCol from 'bootstrap-vue/es/components/layout/col'
import bRow from 'bootstrap-vue/es/components/layout/row'
import bFormGroup from 'bootstrap-vue/es/components/form-group/form-group'
import bFormRadioGroup from 'bootstrap-vue/es/components/form-radio/form-radio-group.js'
import bButton from 'bootstrap-vue/es/components/button/button'
import Vue from 'vue'
import { Getter } from 'vuex-class'
import { AssessmentAnswer } from '../../models/assessment/assessment-answer'
import { QuizAnswer } from '../../models/assessment/quiz-answer'
import { AssessmentMutations } from '../../store/mutations'

@Component({
  template: require('./assessment.html'),
  components: {
    'b-container': bContainer,
    'b-col': bCol,
    'b-row': bRow,
    'b-form-group': bFormGroup,
    'b-form-radio-group': bFormRadioGroup,
    'b-button': bButton
  }
})

export class AssessmentComponent extends Vue {
  @Getter assessmentStarted: boolean
  @Getter assessmentTitle: string
  @Getter assessmentQuestions: Array<AssessmentAnswer>

  answerToOptions (question: AssessmentAnswer) {
    let options = []

    question.QuizAnswers.forEach((answer: QuizAnswer) => {
      options.push({ text: answer.Content, value: answer.Id, checked: (question.AnswerId === answer.Id) })
    })

    return options
  }

  questionGroupName (id: number): string {
    return 'question-group-' + id
  }

  handleChoice (questionId: number, answerId: number) {
    this.$store.commit(AssessmentMutations.ANSWER, { QuestionId: questionId, AnswerId: answerId })
  }

  check () {
    this.$store.dispatch('postAssessment', this.$router)
  }

  mounted () {
    if (!this.assessmentStarted) {
      this.$store.dispatch('loadAssessment', { id: this.$route.params.id, router: this.$router })
    }
  }
}
